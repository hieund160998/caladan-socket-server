var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
const axios = require('axios')
var bodyParser = require('body-parser')

// create application/json parser
var jsonParser = bodyParser.json()

// create application/x-www-form-urlencoded parser

var urlencodedParser = bodyParser.urlencoded({ extended: false })

server.listen(3000);
axios.defaults.baseURL = 'http://103.57.222.114/api/';

// global variables for the server
var enemies = [];
var playerSpawnPoints = [];
var clients = [];
var rooms = ['lobby'];

app.get('/', async(req, res) => {
//        res.send('oke')

        axios.get('item-list').then(resp => {
            res.send(resp.data);
        }).catch(err =>{
                res.send(err);
        });
        return;

});



app.post('/account/login', urlencodedParser, async (req, res) => {
	console.log(req.body.username + ' login')

	await  axios.post( 'login', {
		username: req.body.username,
		password: req.body.password
	}).then(response => {
		console.log(response.data)
		res.send(response.data);

	}).catch(error => {
		console.error(error)
	})

	return;
});

io.on('connection', function(socket) {
	const userId =  guid();
	var authenticated = false;
	var currentPlayer = {};
	currentPlayer.name = 'unknown';
	currentPlayer.room = 'lobby';

	socket.on('handshake', function() {
		console.log('shakehand');
		return;
	});

	socket.on('player connect', function(data) {
		console.log("player connect")
		if (data.token === undefined || data.token.length === 0) {
			console.log(11)
			socket.emit('session timeout', "timeout");

			this.disconnect()
			return;
		}

		console.log(data.token)
		// check authentication
		axios.get("/me", {
				headers: {"Authorization" : `Bearer ${data.token}`}}).then(res => {
				if (res.data.success === true) {
					let value = res.data.data
					currentPlayer = {
						name: value.name,
						room: currentPlayer.room,
						username: value.username,
						id: value.id,
						email: value.email,
						health: 100,
						token: data.token
					};

					authenticated = true
				}

			})
			.catch(error => {
				console.log(error)
				console.log(error.response)
				if (error.response.status === 401) {
					authenticated = false
					socket.emit('session timeout')
					this.disconnect()
				}
			})


		if (data === undefined || !data.hasOwnProperty('room')) {
			data = {}
			data.room = socket.id
		}

		console.log(currentPlayer.name+' recv: player connect to:' +data.room);

		currentPlayer.room = data.room
		socket.join(data.room);

		if (clients[data.room] === undefined) clients[currentPlayer.room] = [];

		for(var i = 0; i < clients[data.room].length;i++) {
			var playerConnected = {
				name: clients[data.room][i].name,
				position: clients[data.room][i].position,
				rotation: clients[data.room][i].position,
				health: clients[data.room][i].health
			};

			// in your current game, we need to tell you about the other players.
			socket.emit('other player connected', playerConnected);
			console.log(currentPlayer.name+' emit: other player connected: '+JSON.stringify(playerConnected));
		}
	});

	socket.on('api get', function(data) {

		let type = (data.item_type === undefined) ? 1 : data.item_type;
		console.log('item::::' + type);

		axios
			.get(data.endpoint, {
				headers: {"Authorization" : `Bearer ${currentPlayer.token}`},
				params: { item_type: type }})
			.then(res => {
				if (data.endpoint === 'user-info') console.log(res.data)

				if (data.endpoint === 'item-list') {
					// console.log('item-list')
					// console.log(res)
					// console.log('item res' . res.data)
					if (data.item_type === 1) socket.emit('item response', res.data)
					if (data.item_type === 2) socket.emit('animal-list response', res.data)
					if (data.item_type === 3) socket.emit('seed-list response', res.data)

				}
				if (data.endpoint === 'upgrade-slot') socket.emit('upgrade-slot response', res.data)

				if (data.endpoint === 'user-info') socket.emit('userinfo response', res.data)

				if (data.endpoint === 'user-inventory') socket.emit('userinventory response', res.data)

				if (data.endpoint === 'animal-list') socket.emit('farm-animal response', res.data)
				if (data.endpoint === 'plant-list') socket.emit('farm-plant response', res.data)
			})
			.catch(error => {
				console.log(error)
				if (error.response.status === 401) {
					authenticated = false
					socket.emit('session timeout', error.response.data.msg)
				}
			})
	});

	socket.on('sell-inventory', function(data) {
		let config = {
			headers: {
				'Authorization': 'Bearer ' + currentPlayer.token
			}
		}

		console.log(data)
		axios
			.post("sell-inventory", data, config)
			.then(res => {
				console.log(`statusCode: ${res.status}`)
				console.log(res.data)
			})
			.catch(error => {
				console.error(error)
				if (error.response.status === 401) {
					authenticated = false
					socket.emit('session timeout', error.response.data.msg)
				}
			})
	});

	socket.on('animal-feed', function(data) {
		let config = {
			headers: {
				'Authorization': 'Bearer ' + currentPlayer.token
			}
		}

		console.log(data)
		axios
			.post("animal-feed", data, config)
			.then(res => {
				console.log(`statusCode: ${res.status}`)
				console.log(res.data)
			})
			.catch(error => {
				console.error(error)
				if (error.response.status === 401) {
					authenticated = false
					socket.emit('session timeout', error.response.data.msg)
				}
			})
	});

	socket.on('animal-collect', function(data) {
		let config = {
			headers: {
				'Authorization': 'Bearer ' + currentPlayer.token
			}
		}

		console.log(data)
		axios
			.post("animal-collect", data, config)
			.then(res => {
				console.log(`statusCode: ${res.status}`)
				console.log(res.data)
			})
			.catch(error => {
				console.error(error)
				if (error.response.status === 401) {
					authenticated = false
					socket.emit('session timeout', error.response.data.msg)
				}
			})
	});

	socket.on('plot-plant', function(data) {
		let config = {
			headers: {
				'Authorization': 'Bearer ' + currentPlayer.token
			}
		}

		console.log(data)
		axios
			.post("plot-plant", data, config)
			.then(res => {
				console.log(`statusCode: ${res.status}`)
				console.log(res.data)
			})
			.catch(error => {
				console.error(error)
				if (error.response.status === 401) {
					authenticated = false
					socket.emit('session timeout', error.response.data.msg)
				}
			})
	});

	socket.on('harvest-plant', function(data) {
		let config = {
			headers: {
				'Authorization': 'Bearer ' + currentPlayer.token
			}
		}

		console.log(data)
		axios
			.post("harvest-plant", data, config)
			.then(res => {
				console.log(`statusCode: ${res.status}`)
				console.log(res.data)
			})
			.catch(error => {
				console.error(error)
				if (error.response.status === 401) {
					authenticated = false
					socket.emit('session timeout', error.response.data.msg)
				}
			})
	});

	socket.on('water-plant', function(data) {
		let config = {
			headers: {
				'Authorization': 'Bearer ' + currentPlayer.token
			}
		}

		console.log(data)
		axios
			.post("water-plant", data, config)
			.then(res => {
				console.log(`statusCode: ${res.status}`)
				console.log(res.data)
			})
			.catch(error => {
				console.error(error)
				if (error.response.status === 401) {
					authenticated = false
					socket.emit('session timeout', error.response.data.msg)
				}
			})
	});

	socket.on('remove-plant', function(data) {
		let config = {
			headers: {
				'Authorization': 'Bearer ' + currentPlayer.token
			}
		}

		console.log(data)
		axios
			.post("remove-plant", data, config)
			.then(res => {
				console.log(`statusCode: ${res.status}`)
				console.log(res.data)
			})
			.catch(error => {
				console.error(error)
				if (error.response.status === 401) {
					authenticated = false
					socket.emit('session timeout', error.response.data.msg)
				}
			})
	});


	socket.on('equip-inventory', function(data) {
		let config = {
			headers: {
				'Authorization': 'Bearer ' + currentPlayer.token
			}
		}

		console.log(data)
		axios
			.post("equip-inventory", data, config)
			.then(res => {
				console.log(`statusCode: ${res.status}`)
				console.log(res.data)
				socket.emit('userinventory response', res.data)
			})
			.catch(error => {
				console.error(error)
				if (error.response.status === 401) {
					authenticated = false
					socket.emit('session timeout', error.response.data.msg)
				}
			})
	});

	socket.on('purchase', function(data) {
		// if (!authenticated) {
		// 	socket.emit('session timeout', "You need to login first!")
		// 	return;
		// }
		let config = {
			headers: {
				'Authorization': 'Bearer ' + currentPlayer.token
			}
		}

		axios
			.post("user-purchase", data, config)
			.then(res => {
				console.log(`statusCode: ${res.status}`)
				console.log(res.data)

				//socket.emit('userinfo response', res.data.userinfo)
			})
			.catch(error => {
				console.error(error)
				if (error.response.status === 401) {
					authenticated = false
					socket.emit('session timeout', error.response.data.msg)
				}
			})
	});

	socket.on('play', function(data) {
		if (!authenticated) {
			socket.emit('session timeout', "You need to login first!")
			return;
		}

		console.log(currentPlayer.name+' recv: play: '+JSON.stringify(data));
		// if this is the first person to join the game init the enemies
		if(clients[currentPlayer.room] === undefined ) clients[currentPlayer.room] = [];

		if(clients[currentPlayer.room].length === 0) {
			numberOfEnemies = data.enemySpawnPoints.length;
			enemies = [];
			data.enemySpawnPoints.forEach(function(enemySpawnPoint) {
				var enemy = {
					name: guid(),
					position: enemySpawnPoint.position,
					rotation: enemySpawnPoint.rotation,
					health: 100
				};
				enemies.push(enemy);
			});

			playerSpawnPoints = [];
			data.playerSpawnPoints.forEach(function(_playerSpawnPoint) {
				var playerSpawnPoint = {
					position: _playerSpawnPoint.position,
					rotation: _playerSpawnPoint.rotation
				};
				playerSpawnPoints.push(playerSpawnPoint);
			});
		}

		var enemiesResponse = {
			enemies: enemies
		};

		// we always will send the enemies when the player joins
		console.log(currentPlayer.name+' emit: enemies: '+JSON.stringify(enemiesResponse));
		socket.emit('enemies', enemiesResponse);
		var randomSpawnPoint = playerSpawnPoints[Math.floor(Math.random() * playerSpawnPoints.length)];

		currentPlayer.position = randomSpawnPoint.position
		currentPlayer.rotation = randomSpawnPoint.rotation

		clients[currentPlayer.room].push(currentPlayer);
		// in your current game, tell you that you have joined
		console.log(currentPlayer.name+' emit: ' +currentPlayer.room+' play: '+JSON.stringify(currentPlayer));

		if (currentPlayer.room !== 'FarmShop' && currentPlayer.room !== 'FarmShop2') {
			socket.emit('play', currentPlayer);
			// in your current game, we need to tell the other players about you.
			socket.to(currentPlayer.room).emit('other player connected', currentPlayer);
		}

	});

	socket.on('player move', function(data) {
		// if (!authenticated) {
		// 	socket.emit('session timeout', "You need to login first!")
		// 	return;
		// }

//		console.log('recv: '+currentPlayer.room+'-move: '+JSON.stringify(data));
		currentPlayer.position = data.position;
		socket.to(currentPlayer.room).emit('player move', data);
	});

	socket.on('player turn', function(data) {
		console.log('recv: turn: '+JSON.stringify(data));
		currentPlayer.rotation = data.rotation;
		socket.broadcast.emit('player turn', currentPlayer);
	});

	socket.on('player shoot', function() {
		console.log(currentPlayer.name+' recv: shoot');
		var data = {
			name: currentPlayer.name
		};
		console.log(currentPlayer.name+' bcst: shoot: '+JSON.stringify(data));
		socket.emit('player shoot', data);
		socket.broadcast.emit('player shoot', data);
	});

	socket.on('health', function(data) {
		console.log(currentPlayer.name+' recv: health: '+JSON.stringify(data));
		// only change the health once, we can do this by checking the originating player
		if(data.from === currentPlayer.name) {
			var indexDamaged = 0;
			if(!data.isEnemy) {
				clients = clients.map(function(client, index) {
					if(client.name === data.name) {
						indexDamaged = index;
						client.health -= data.healthChange;
					}
					return client;
				});
			} else {
				enemies = enemies.map(function(enemy, index) {
					if(enemy.name === data.name) {
						indexDamaged = index;
						enemy.health -= data.healthChange;
					}
					return enemy;
				});
			}

			var response = {
				name: (!data.isEnemy) ? clients[indexDamaged].name : enemies[indexDamaged].name,
				health: (!data.isEnemy) ? clients[indexDamaged].health : enemies[indexDamaged].health
			};
			console.log(currentPlayer.name+' bcst: health: '+JSON.stringify(response));
			socket.emit('health', response);
			socket.broadcast.emit('health', response);
		}
	});

	socket.on('disconnect', function() {
		console.log(currentPlayer.name+' recv: disconnect from '+currentPlayer.room);
		socket.to(currentPlayer.room).emit('other player disconnected', currentPlayer);
		console.log(currentPlayer.name+' bcst: other player disconnected '+JSON.stringify(currentPlayer));

		if (clients[currentPlayer.room] === undefined) clients[currentPlayer.room] = [];
		for(var i = 0; i < clients[currentPlayer.room].length; i++) {
			if (clients[currentPlayer.room][i].name === currentPlayer.name) {
				clients[currentPlayer.room].splice(i,1);
			}
		}

		socket.leave(currentPlayer.room);
		currentPlayer.room = 'lobby';
	});

});

console.log('--- server is running ...');

function guid() {
	function s4() {
		return Math.floor((1+Math.random()) * 0x10000).toString(16).substring(1);
	}
	return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}
